#!/bin/bash

# Source directory path
source_dir="/opt/axway/Axway-7.7.0/apigateway/logs/opentraffic"

# Destination server IP
destination_ip="192.168.226.29"

# Destination directory path on the remote server
destination_dir="/path/to/destination_directory/"

# Get today's date in YYYYMMDD format
today=$(date +"%Y%m%d")

# Find files older than today and transfer them to the remote server
find "$source_dir" -type f ! -newermt "$today-00:00" -exec scp {} "username@$destination_ip:$destination_dir" \;
